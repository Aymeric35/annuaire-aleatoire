import { User } from "./user";
import axios from "axios";
import { renderUser } from "./user";
import { createIcons, icons } from 'lucide';

const USERS = [];
createIcons({ icons })

const addUser = document.querySelectorAll('.add-user');
const usersContainer = document.querySelector('.users__card.add-user');

addUser.forEach(user => {
    user.addEventListener('click', () => {
        axios.get("https://randomuser.me/api/").then((response) => {
            let fetchedUser = response.data.results[0];
            let params = [
                fetchedUser.name.first,
                fetchedUser.name.last,
                fetchedUser.dob.age,
                fetchedUser.picture.large,
                fetchedUser.location.country,
                fetchedUser.email
            ];
            let user = new User(...params);
            USERS.push(user);
            usersContainer.insertAdjacentHTML('beforebegin', renderUser(user))
            createIcons({ icons })
        }).catch((error) => {
            console.log(error);
        })
    })
})
