export class User {
    constructor(firstname, lastname, age, picture, country, email) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.picture = picture;
        this.country = country;
        this.email = email;
    }

    get fullname() {
        return `${this.firstname} ${this.lastname}`;
    }
}

export function renderUser(user) {
    const card =
        `<div class="users__image-container">` +
            `<img src="${user.picture}"></img>` +
        `</div>` +
        `<div class="users__card-body">` +
            `<h3> ${user.fullname} </h3>` +
            `<div class="users__body-content">` +
                `<p class="users__age"> <i icon-name="gift"></i> Age : ${user.age} </p>` +
                `<p class="users__address"> <i icon-name="map"></i> Adresse : ${user.country} </p>` +
                `<p class="users__email"> <i icon-name="mail"></i> Email : ${user.email} </p>` +
            `</div>` +
        `</div>`
    return `<div class="users__card"> ${card} </div>`
}